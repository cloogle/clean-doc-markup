# clean-doc-markup

A JavaScript parser for markdown-like markup in [Clean][] documentation.

This package can be downloaded from [npm][on-npm]:

	npm install clean-doc-markup

## Author &amp; license
clean-doc-markup is written and maintained by [Camil Staps][].

This project is licensed under AGPL v3 with additional terms under section 7;
see the [LICENSE](/LICENSE) file for details.

[Camil Staps]: https://camilstaps.nl
[Clean]: http://clean.cs.ru.nl
[on-npm]: https://npmjs.com/package/clean-doc-markup
