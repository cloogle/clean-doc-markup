/**
 * Copyright 2018-2021 Camil Staps.
 *
 * This file is part of clean-doc-markup.
 *
 * Clean-doc-markup is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Clean-doc-markup is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with clean-doc-markup. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

/**
 * Replace all strings that look like HTTP(S) URLs to simple HTML links.
 */
Element.prototype.replaceURLs = function() {
	var childNodes = 'content' in this ? this.content.childNodes : this.childNodes;
	for (var i = 0; i < childNodes.length; i++) {
		var node = childNodes[i];
		if (node.nodeType == Node.TEXT_NODE) {
			var newValue = node.nodeValue.replace(/(https?:\/\/(?:[^\s.)>]|\.(?!\s|$))+)/g, '<a href="$1" target="_blank">$1</a>');
			if (newValue != node.nodeValue) {
				var newElement = document.createElement('span');
				newElement.innerHTML = newValue;
				if ('content' in this)
					this.content.replaceChild(newElement, node);
				else
					this.replaceChild(newElement, node);
			}
		} else {
			node.replaceURLs();
		}
	}
	if ('content' in this)
		this.content.childNodes = childNodes;
	else
		this.childNodes = childNodes;
};

/**
 * Add documentation links for {{..}} markup.
 * replace_fun is a function that receives the content of the link and returns
 * a HTML string to replace it.
 */
String.prototype.addLinks = function(replace_fun) {
	return this.replace(/{{([^}]+)}}/g, function(s, link) {
		return replace_fun(link);
	});
};

/**
 * Escape special HTML characters.
 */
String.prototype.escapeHTML = function () {
	const text = document.createTextNode(this);
	const p = document.createElement('p');
	p.appendChild(text);
	return p.innerHTML;
};

/**
 * Simplified markdown to HTML converter.
 * Normally you should use the wrapper markup() defined below.
 */
String.prototype._markup = function(highlightCallback, link_replace_fun) {
	return this
		.replace(/((?:^|\n)```[^\n]*\n|`` |`|\n\s*-|\n\n|\*{1,3}(?=\w)|\n\s*-|{{|[<>])([\s\S]*)/,
			function(s, m, rest) {
				switch (m) {
					case '`':
						var i = rest.indexOf('`');
						if (i<0) return m + rest._markup(highlightCallback, link_replace_fun);
						var code = rest.slice(0,i).escapeHTML().addLinks(link_replace_fun);
						return '<code>' + code + '</code>' + rest.slice(i+1)._markup(highlightCallback, link_replace_fun);
					case '`` ':
						var i = rest.indexOf(' ``');
						if (i<0) return m + rest._markup(highlightCallback, link_replace_fun);
						var code = rest.slice(0,i).escapeHTML().addLinks(link_replace_fun);
						return '<code>' + code + '</code>' + rest.slice(i+3)._markup(highlightCallback, link_replace_fun);
					case '\n\n':
						return '<br class="parbreak"/>' + rest._markup(highlightCallback, link_replace_fun);
					case '*':
						var i = rest.indexOf('*');
						if (i<0) return m + rest._markup(highlightCallback, link_replace_fun);
						return '<em>' + rest.slice(0,i).escapeHTML() + '</em>' + rest.slice(i+1)._markup(highlightCallback, link_replace_fun);
					case '**':
						var i = rest.indexOf('**');
						if (i<0) return m + rest._markup(highlightCallback, link_replace_fun);
						return '<strong>' + rest.slice(0,i).escapeHTML() + '</strong>' + rest.slice(i+2)._markup(highlightCallback, link_replace_fun);
					case '***':
						var i = rest.indexOf('***');
						if (i<0) return m + rest._markup(highlightCallback, link_replace_fun);
						return '<strong><em>' + rest.slice(0,i).escapeHTML() + '</em></strong>' + rest.slice(i+3)._markup(highlightCallback, link_replace_fun);
					case '{{':
						var i = rest.indexOf('}}');
						if (i<0) return m + rest._markup(highlightCallback, link_replace_fun);
						return link_replace_fun(rest.slice(0,i)) + rest.slice(i+2)._markup(highlightCallback, link_replace_fun);
					case '<':
						return '&lt;' + rest._markup(highlightCallback, link_replace_fun);
					case '>':
						return '&gt;' + rest._markup(highlightCallback, link_replace_fun);
				}

				if (m.match(/\n\s*-/))
					return '\n<br/>-' + rest._markup(highlightCallback, link_replace_fun);
				else if (m.match(/\n\s*\*/))
					return '\n<br/>*' + rest._markup(highlightCallback, link_replace_fun);

				if (m.slice(0,3) == '```') {
					var i = rest.indexOf('\n```');
					var code = rest.slice(0,i);
					rest = rest.slice(i+4).trim()._markup(highlightCallback, link_replace_fun);
					switch (m.slice(3)) {
						case 'clean\n':
							return '<pre>' + highlightClean(code, highlightCallback) + '</pre>' + rest;
						default:
							return '<pre>' + code.escapeHTML() + '</pre>' + rest;
					}
				}
			});
};

/**
 * Simplified markdown to HTML converter.
 * - highlightCallback is a callback for the Clean highlighter.
 * - link_replace_fun is a replace_fun for addLinks(); see above.
 */
String.prototype.markup = function(highlightCallback, link_replace_fun) {
	var template = document.createElement('template');
	template.innerHTML = this.replace(/{{`([^`}]+)`}}/g, '`{{$1}}`')._markup(highlightCallback, link_replace_fun);
	template.replaceURLs();
	return template.innerHTML;
};
